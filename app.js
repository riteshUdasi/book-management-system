class Book{
    constructor(title,author,isbn){
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }
}

class UI{
    addBookToList(book){
        const bookList = document.getElementById("book-list");
        const row = document.createElement("tr");
        row.innerHTML = `
        <td>${book.title}</td>
        <td>${book.author}</td>
        <td>${book.isbn}</td>
        <td><a href="#" class="delete">X</a></td>
        `;
        bookList.appendChild(row);
    }
    
    deleteBook(target){
        target.parentElement.parentElement.remove();
    }
    clearFields(){
        document.getElementById("title").value = "";
        document.getElementById("author").value = "";
        document.getElementById("isbn").value = "";
    }
    
    //< div class = "alert error">Hello World!</div> 
    showAlert(message, className){
        const div = document.createElement('div');
        div.className = `alert ${className}`;
        div.appendChild(document.createTextNode(message));
        
        const container = document.querySelector(".container");
        const form = document.querySelector("#book-form");
        container.insertBefore(div,form);
        
        setTimeout(function(){
            document.querySelector('.alert').remove();
        },3000);
    }
}
class StorageHandler{
    static getBooks(){
        let books;
        if(localStorage.getItem("books") === null){
            books = [];
        }else{
            books = JSON.parse(localStorage.getItem("books"));
        }
        return books;
    }
    
    static displayBooks(){
        const books = StorageHandler.getBooks();
        const ui = new UI();
        books.forEach(function(book){
            ui.addBookToList(book);
        });
    }
    
    static removeBook(isbn){
        const books = StorageHandler.getBooks();
        books.forEach(function(book,index){
            if(book.isbn ===isbn){
                books.splice(index,1);
            }
        });
        localStorage.setItem("books",JSON.stringify(books));
    }
    
    static addBook(book){
        const books = StorageHandler.getBooks();
        books.push(book);
        localStorage.setItem("books",JSON.stringify(books));
    }
}


document.addEventListener("DOMContentLoaded",StorageHandler.displayBooks);

document.querySelector("#book-form").addEventListener('submit',formSubmit);
function formSubmit(e){
    e.preventDefault();
    const title = document.getElementById("title").value,
          author = document.getElementById("author").value,
          isbn = document.getElementById("isbn").value;
    
    const ui = new UI();
    if(title ==="" || author ==="" || isbn ==="" ){
        ui.showAlert("Please fill in all fields!","error");
    }else{
        book = new Book(title,author,isbn);
        ui.addBookToList(book);
        StorageHandler.addBook(book);
        ui.clearFields();
        ui.showAlert("Book Added Successfully!","success");
    }
}

document.getElementById("book-list").addEventListener('click',deleteBookEvent);

function deleteBookEvent(e){
    const ui = new UI();
    if(e.target.classList.contains("delete")){
        ui.deleteBook(e.target);
        StorageHandler.removeBook(e.target.parentElement.previousElementSibling.textContent);
        ui.showAlert("Book Deleted!","success");
        e.preventDefault();
    }
}



























